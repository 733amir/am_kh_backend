drop table if exists tokens;
drop table if exists users;

create table users (
    id int auto_increment primary key,
    email varchar(128) unique not null,
    signed_in_with_google boolean not null,
    full_name varchar(128),
    address varchar(512),
    telephone varchar(15),
    salt varbinary(32),
    hashed_password varbinary(32)
)  engine=innodb;

create table tokens (
    id int auto_increment primary key,
    token varchar(256) not null unique,
    user_id int not null,
    valid_until datetime,
    foreign key (user_id) references users (id)
) engine=innodb;
