package main

import (
	"log"
	"os"

	"net/http"
)

func main() {

	mail := NewSimpleMailSender(os.Getenv("MAIL_FROM"), os.Getenv("MAIL_PASS"))

	repo, err := NewRepo("amkh:amkh@tcp(127.0.0.1:33306)/amkh?parseTime=true&clientFoundRows=true")
	if err != nil {
		log.Fatal(err)
	}

	usecase := Usecase{
		Repo:                      repo,
		Mail:                      mail,
		FrontEndPasswordResetLink: os.Getenv("FRONTEND_PASSWORD_RESET_LINK"),
		Audience:                  os.Getenv("GOOGLE_CLIENT_ID"),
	}

	api := NewAPI(usecase)

	log.Print("listening ...")
	if err := http.ListenAndServe("127.0.0.1:8080", &api.Mux); err != nil {
		log.Fatalf("listen and serve: %v", err)
	}
}
