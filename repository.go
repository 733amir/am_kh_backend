package main

import (
	"context"
	"fmt"
	"time"

	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

type Repo struct {
	db *sql.DB
}

type RepoError struct {
	Err error
}

func (e RepoError) Error() string {
	return "repository error"
}

func (e RepoError) Unwrap() error {
	return e.Err
}

func NewRepo(source string) (Repo, error) {
	db, err := sql.Open("mysql", source)
	if err != nil {
		return Repo{}, fmt.Errorf("repo: %w", err)
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return Repo{db}, nil
}

func (repo Repo) NewUser(
	ctx context.Context,
	user User,
) (int, error) {
	res, err := repo.db.ExecContext(
		ctx,
		"insert into users(email, full_name, signed_in_with_google, address, telephone, salt, hashed_password) values (?, ?, ?, ?, ?, ?, ?)",
		user.Email, user.FullName, user.SignedInWithGoogle, user.Address, user.Telephone, user.Salt, user.HashedPassword,
	)
	if err != nil {
		return 0, RepoError{fmt.Errorf("new user: %w", err)}
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, RepoError{fmt.Errorf("new user: id: %w", err)}
	}
	return int(id), nil
}

func (repo Repo) FindUserByEmail(
	ctx context.Context,
	email string,
) (User, error) {
	rows, err := repo.db.QueryContext(
		ctx,
		"select id, email, signed_in_with_google, full_name, address, telephone, salt, hashed_password from users where email = ?",
		email,
	)
	if err != nil {
		return User{}, RepoError{fmt.Errorf("find user by email: query: %w", err)}
	}

	if !rows.Next() {
		return User{}, ErrNotFound
	}

	var user User
	err = rows.Scan(
		&user.Id, &user.Email, &user.SignedInWithGoogle, &user.FullName, &user.Address,
		&user.Telephone, &user.Salt, &user.HashedPassword,
	)
	if err != nil {
		return User{}, RepoError{fmt.Errorf("find user by email: scanning: %w", err)}
	}

	return user, nil
}

func (repo Repo) FindUserById(
	ctx context.Context,
	id int,
) (User, error) {
	rows, err := repo.db.QueryContext(
		ctx,
		"select id, email, signed_in_with_google, full_name, address, telephone, salt, hashed_password from users where id = ?",
		id,
	)
	if err != nil {
		return User{}, RepoError{fmt.Errorf("find user by id: query: %w", err)}
	}

	if !rows.Next() {
		return User{}, ErrNotFound
	}

	var user User
	err = rows.Scan(
		&user.Id, &user.Email, &user.SignedInWithGoogle, &user.FullName, &user.Address,
		&user.Telephone, &user.Salt, &user.HashedPassword,
	)
	if err != nil {
		return User{}, RepoError{fmt.Errorf("find user by id: scanning: %w", err)}
	}

	return user, nil
}

func (repo Repo) UpdateUserData(
	ctx context.Context,
	user User,
) error {
	res, err := repo.db.ExecContext(
		ctx,
		"update users set email=?, full_name=?, address=?, telephone=? where id = ?",
		user.Email, user.FullName, user.Address, user.Telephone, user.Id,
	)
	if err != nil {
		return RepoError{fmt.Errorf("update user data: %w", err)}
	}

	if count, err := res.RowsAffected(); err != nil {
		return RepoError{fmt.Errorf("update user data: rows affected: %w", err)}
	} else if count == 0 {
		return ErrNotFound
	}

	return nil
}

func (repo Repo) UpdateUserPassword(
	ctx context.Context,
	user User,
) error {
	res, err := repo.db.ExecContext(
		ctx,
		"update users set salt=?, hashed_password=? where id = ?",
		user.Salt, user.HashedPassword, user.Id,
	)
	if err != nil {
		return RepoError{fmt.Errorf("update user password: %w", err)}
	}

	if count, err := res.RowsAffected(); err != nil {
		return RepoError{fmt.Errorf("update user password: rows affected: %w", err)}
	} else if count == 0 {
		return ErrNotFound
	}

	return nil
}

func (repo Repo) NewToken(
	ctx context.Context,
	token Token,
) (int, error) {
	res, err := repo.db.ExecContext(
		ctx,
		"insert into tokens(token, user_id, valid_until) values (?, ?, ?)",
		token.Token, token.UserId, token.ValidUntil,
	)
	if err != nil {
		return 0, RepoError{fmt.Errorf("new token: %w", err)}
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, RepoError{fmt.Errorf("new token: id: %w", err)}
	}

	return int(id), nil
}

func (repo Repo) FindToken(
	ctx context.Context,
	token string,
) (Token, error) {
	rows, err := repo.db.QueryContext(
		ctx,
		"select id, token, user_id, valid_until from tokens where token = ?",
		token,
	)
	if err != nil {
		return Token{}, RepoError{fmt.Errorf("find token: query: %w", err)}
	}

	if !rows.Next() {
		return Token{}, ErrNotFound
	}

	var t Token
	err = rows.Scan(&t.Id, &t.Token, &t.UserId, &t.ValidUntil)
	if err != nil {
		return Token{}, RepoError{fmt.Errorf("find token: scanning: %w", err)}
	}

	return t, nil
}

func (repo Repo) ExpireToken(
	ctx context.Context,
	token string,
) error {
	_, err := repo.db.ExecContext(
		ctx,
		"update tokens set valid_until=? where token=?",
		time.Now(), token,
	)
	if err != nil {
		return RepoError{fmt.Errorf("expire token: %w", err)}
	}

	return nil
}
