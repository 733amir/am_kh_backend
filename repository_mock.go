package main

import (
	"context"

	_ "github.com/go-sql-driver/mysql"
)

type RepoMock struct {
	users  map[string]User
	tokens []Token
}

func NewRepoMock() RepoMock {
	return RepoMock{
		users:  make(map[string]User, 100),
		tokens: make([]Token, 0, 100),
	}
}

func (repo RepoMock) NewUser(
	_ context.Context,
	user User,
) (int, error) {
	if _, ok := repo.users[user.Email]; ok {
		return 0, RepoError{ErrAlreadyExists}
	}

	user.Id = len(repo.users) + 1
	repo.users[user.Email] = user

	return user.Id, nil
}

func (repo RepoMock) FindUserByEmail(
	_ context.Context,
	email string,
) (User, error) {
	user, ok := repo.users[email]
	if !ok {
		return User{}, ErrNotFound
	}
	return user, nil
}

func (repo RepoMock) NewToken(
	_ context.Context,
	token Token,
) (int, error) {
	token.Id = len(repo.tokens) + 1
	repo.tokens = append(repo.tokens, token)
	return token.Id, nil
}

func (repo RepoMock) FindToken(
	_ context.Context,
	token string,
) (Token, error) {
	for _, t := range repo.tokens {
		if t.Token != token {
			continue
		}

		return t, nil
	}

	return Token{}, ErrNotFound
}
