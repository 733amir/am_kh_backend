package main

import (
	"errors"
	"log"

	"encoding/json"
	"net/http"
)

type API struct {
	Mux     http.ServeMux
	usecase Usecase
}

func NewAPI(usecase Usecase) *API {
	api := new(API)

	api.Mux.HandleFunc("/signup", api.signup)
	api.Mux.HandleFunc("/login", api.login)
	api.Mux.HandleFunc("/logout", api.logout)
	api.Mux.HandleFunc("/profile", api.profile)
	api.Mux.HandleFunc("/profile/password/reset-link", api.resetLink)
	api.Mux.HandleFunc("/profile/password", api.resetPassword)
	api.usecase = usecase

	return api
}

type signUpRequestData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	IdToken  string `json:"id_token"`
}

type signUpResponseData struct {
	Token string `json:"token"`
}

func (api *API) signup(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "only POST http method is allowed", http.StatusMethodNotAllowed)
		return
	}

	var requestData signUpRequestData
	if err := json.NewDecoder(r.Body).Decode(&requestData); err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	var token string
	var err error
	if requestData.IdToken != "" {
		token, err = api.usecase.GoogleIdToken(r.Context(), requestData.IdToken)
	} else {
		token, err = api.usecase.SignUp(r.Context(), requestData.Email, requestData.Password)
	}
	var rerr RepoError
	if errors.As(err, &rerr) {
		log.Printf("error: %v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(signUpResponseData{Token: token})
	return
}

type loginRequestData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	IdToken  string `json:"id_token"`
}

type loginResponseData struct {
	Token string `json:"token"`
}

func (api *API) login(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "only POST http method is allowed", http.StatusMethodNotAllowed)
		return
	}

	var requestData loginRequestData
	if err := json.NewDecoder(r.Body).Decode(&requestData); err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	var token string
	var err error
	if requestData.IdToken != "" {
		token, err = api.usecase.GoogleIdToken(r.Context(), requestData.IdToken)
	} else {
		token, err = api.usecase.Login(r.Context(), requestData.Email, requestData.Password)
	}
	var rerr RepoError
	if errors.As(err, &rerr) {
		log.Printf("error: %v", rerr.Err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	} else if err == ErrNotFound {
		http.Error(w, "email not found", http.StatusNotFound)
		return
	} else if err == ErrWrongPassword {
		http.Error(w, "wrong password", http.StatusUnauthorized)
		return
	} else if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(loginResponseData{Token: token})
	return
}

func getToken(r *http.Request) string {
	bearerToken := r.Header.Get("Authorization")
	if len(bearerToken) < 8 {
		return ""
	}

	return bearerToken[7:]
}

func (api *API) logout(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "only POST http method is allowed", http.StatusMethodNotAllowed)
		return
	}

	api.usecase.Logout(r.Context(), getToken(r))
	w.WriteHeader(http.StatusOK)
	return
}

type profileData struct {
	Email     string `json:"email"`
	FullName  string `json:"fullname"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
}

func (api *API) profile(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost && r.Method != http.MethodGet {
		http.Error(w, "only GET,POST http methods are allowed", http.StatusMethodNotAllowed)
		return
	}

	user, err := api.usecase.Authorize(r.Context(), getToken(r))
	var rerr RepoError
	if errors.As(err, &rerr) {
		log.Printf("error: %v", rerr.Err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	} else if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	if r.Method == http.MethodPost {
		var requestData profileData
		if err := json.NewDecoder(r.Body).Decode(&requestData); err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		err := api.usecase.UpdateUser(
			r.Context(),
			user,
			requestData.Email,
			requestData.FullName,
			requestData.Address,
			requestData.Telephone,
		)
		var rerr RepoError
		if errors.As(err, &rerr) {
			log.Printf("error: %v", rerr.Err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		} else if err == ErrNotFound {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		} else if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		user.Email = requestData.Email
		user.FullName = requestData.FullName
		user.Address = requestData.Address
		user.Telephone = requestData.Telephone
	}

	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(profileData{
		Email:     user.Email,
		FullName:  user.FullName,
		Address:   user.Address,
		Telephone: user.Telephone,
	})
	return
}

type resetLinkRequestData struct {
	Email string `json:"email"`
}

func (api *API) resetLink(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "only POST http methods are allowed", http.StatusMethodNotAllowed)
		return
	}

	var requestData resetLinkRequestData
	if err := json.NewDecoder(r.Body).Decode(&requestData); err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	err := api.usecase.SendResetLink(r.Context(), requestData.Email)
	var rerr RepoError
	if errors.As(err, &rerr) {
		log.Printf("error: %v", rerr.Err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	} else if err == ErrNotFound {
		http.Error(w, "email not found", http.StatusNotFound)
		return
	} else if err != nil {
		log.Printf("error: %v", errors.Unwrap(err))
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

type resetPasswordRequestData struct {
	NewPassword string `json:"new_password"`
}

func (api *API) resetPassword(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "only POST http method is allowed", http.StatusMethodNotAllowed)
		return
	}

	user, err := api.usecase.Authorize(r.Context(), getToken(r))
	var rerr RepoError
	if errors.As(err, &rerr) {
		log.Printf("error: %v", rerr.Err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	} else if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	var requestData resetPasswordRequestData
	if err := json.NewDecoder(r.Body).Decode(&requestData); err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	err = api.usecase.UpdatePassword(r.Context(), user.Id, requestData.NewPassword)
	if errors.As(err, &rerr) {
		log.Printf("error: %v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	} else if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}
