package main

import (
	"context"
	"testing"
)

const testSource = "amkh:amkh@tcp(127.0.0.1:33306)/amkh_test"

func TestRepo(t *testing.T) {
	repo, err := NewRepo(testSource)
	if err != nil {
		t.Fatalf("%v", err)
	}

	t.Run("add a token without a user", func(t *testing.T) {
		tt, err := GenerateRandomStringURLSafe(24)
		if err != nil {
			t.Fatal(err)
		}

		token := Token{
			Token:  tt,
			UserId: -1,
		}

		_, err = repo.NewToken(context.Background(), token)
		if err == nil {
			t.Fatalf("should not be able to create a token without a user")
		}
	})

	t.Run("adding a new user", func(t *testing.T) {
		user := User{
			Email:          "me@example.com",
			FullName:       "me myself",
			Address:        "here and there",
			Telephone:      "15555555",
			Salt:           []byte{100, 100},
			HashedPassword: []byte{100, 100},
		}

		id, err := repo.NewUser(context.Background(), user)
		if err != nil {
			t.Fatal(err)
		}
		if id == 0 {
			t.Fatalf("expected not zero value")
		}
		user.Id = id

		t.Run("retrieving user's information", func(t *testing.T) {
			got, err := repo.FindUserByEmail(context.Background(), user.Email)
			if err != nil {
				t.Fatal(err)
			}

			if got.Email != user.Email || got.FullName != user.FullName || got.Address != user.Address ||
				got.Telephone != user.Telephone || string(got.Salt) != string(user.Salt) ||
				string(got.HashedPassword) != string(user.HashedPassword) {
				t.Fatalf("got %v expectd %v", got, user)
			}
		})

		t.Run("add a token", func(t *testing.T) {
			tt, err := GenerateRandomStringURLSafe(24)
			if err != nil {
				t.Fatal(err)
			}

			token := Token{
				Token:  tt,
				UserId: user.Id,
			}

			id, err := repo.NewToken(context.Background(), token)
			if err != nil {
				t.Fatal(err)
			}
			if id == 0 {
				t.Fatalf("expected not zero value")
			}

			t.Run("retrieving token's information", func(t3 *testing.T) {
				got, err := repo.FindToken(context.Background(), token.Token)
				if err != nil {
					t.Fatal(err)
				}

				if got.Token != token.Token || got.UserId != token.UserId {
					t.Fatalf("got %v expectd %v", got, token)
				}
			})
		})
	})
}
