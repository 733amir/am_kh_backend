package main

import (
	"net/smtp"
)

type Mailer interface {
	Send(to string, msg string) error
}

type MailError struct {
	err error
}

func (err MailError) Error() string {
	return "Mail error"
}

func (err MailError) Unwrap() error {
	return err.err
}

const (
	smtpHost = "smtp.gmail.com"
	smtpPort = "587"
)

type SimpleMail struct {
	owner string
	auth  smtp.Auth
}

func NewSimpleMailSender(owner, password string) Mailer {
	return SimpleMail{
		owner: owner,
		auth:  smtp.PlainAuth("", owner, password, smtpHost),
	}
}

func (m SimpleMail) Send(to string, msg string) error {
	err := smtp.SendMail(smtpHost+":"+smtpPort, m.auth, m.owner, []string{to}, []byte(msg))
	if err != nil {
		return MailError{err}
	}
	return nil
}
