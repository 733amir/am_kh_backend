package main

import (
	"errors"
	"regexp"
	"time"

	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
)

type User struct {
	Id                 int
	Email              string
	SignedInWithGoogle bool
	FullName           string
	Address            string
	Telephone          string
	Salt               []byte
	HashedPassword     []byte
}

type Token struct {
	Id         int
	Token      string
	UserId     int
	ValidUntil *time.Time
}

const TokenSize = 72

var (
	ErrNotFound          = errors.New("not found")
	ErrAlreadyExists     = errors.New("already exists")
	ErrExpired           = errors.New("expired")
	ErrCannotChangeEmail = errors.New("can't change your email address")
)

type SecureRandomGeneratorError struct {
	Err error
}

func (e SecureRandomGeneratorError) Error() string {
	return "problem with secure random generator"
}

func (e SecureRandomGeneratorError) Unwrap() error {
	return e.Err
}

// GenerateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, SecureRandomGeneratorError{err}
	}

	return b, nil
}

// GenerateRandomStringURLSafe returns a URL-safe, base64 encoded
// securely generated random string.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomStringURLSafe(n int) (string, error) {
	b, err := GenerateRandomBytes(n)
	return base64.URLEncoding.EncodeToString(b), err
}

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")
var ErrInvalidEmail = errors.New("invalid email address")

func ValidateEmail(email string) (string, error) {
	if !emailRegex.MatchString(email) {
		return "", ErrInvalidEmail
	}

	return email, nil
}

var passwordLengthRegex = regexp.MustCompile("^[A-Za-z\\d]{8,}$")
var passwordLetterRegex = regexp.MustCompile("[A-Za-z]")
var passwordNumberRegex = regexp.MustCompile("[\\d]")
var ErrInvalidPassword = errors.New("password must be at least 8 character including one letter and one number")

func ValidatePassword(password string) (string, error) {
	if !passwordLengthRegex.MatchString(password) ||
		!passwordLetterRegex.MatchString(password) ||
		!passwordNumberRegex.MatchString(password) {
		return "", ErrInvalidPassword
	}

	return password, nil
}

func HashPassword(salt []byte, password string) []byte {
	h := sha256.Sum256(append(salt, []byte(password)...))
	return h[:]
}

func SaltedHashedPassword(password string) (salt, hashedPassword []byte, err error) {
	salt, err = GenerateRandomBytes(32)
	if err != nil {
		return nil, nil, err
	}

	return salt, HashPassword(salt, password), nil
}

var ErrWrongPassword = errors.New("wrong password")

func CheckPassword(salt []byte, password string, hashedPassword []byte) bool {
	h := HashPassword(salt, password)

	if len(h) != len(hashedPassword) {
		return false
	}

	equal := true
	for i := range hashedPassword {
		equal = equal && (h[i] == hashedPassword[i])
	}
	return equal
}

type ThirdPartyError struct {
	err error
}

func (err ThirdPartyError) Error() string {
	return "3rd party error"
}

func (err ThirdPartyError) Unwrap() error {
	return err.err
}
