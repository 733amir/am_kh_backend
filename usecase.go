package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"google.golang.org/api/idtoken"
)

type RepoInterface interface {
	NewUser(ctx context.Context, user User) (int, error)
	FindUserById(ctx context.Context, id int) (User, error)
	FindUserByEmail(ctx context.Context, email string) (User, error)
	NewToken(ctx context.Context, token Token) (int, error)
	FindToken(ctx context.Context, token string) (Token, error)
	UpdateUserData(ctx context.Context, user User) error
	UpdateUserPassword(ctx context.Context, user User) error
	ExpireToken(ctx context.Context, token string) error
}

type Usecase struct {
	Repo RepoInterface
	Mail Mailer

	FrontEndPasswordResetLink string
	Audience                  string
}

func (usecase Usecase) SignUp(
	ctx context.Context,
	email, password string,
) (token string, err error) {
	email, err = ValidateEmail(email)
	if err != nil {
		return "", err
	}

	user, err := usecase.Repo.FindUserByEmail(ctx, email)
	if err != nil && err != ErrNotFound {
		return "", err
	}

	if user.Email == email {
		return "", ErrAlreadyExists
	}

	password, err = ValidatePassword(password)
	if err != nil {
		return "", err
	}

	salt, hashedPassword, err := SaltedHashedPassword(password)
	if err != nil {
		return "", err
	}

	user = User{
		Email:          email,
		Salt:           salt,
		HashedPassword: hashedPassword,
	}
	user.Id, err = usecase.Repo.NewUser(ctx, user)
	if err != nil {
		return "", err
	}

	t := Token{
		UserId: user.Id,
	}
	t.Token, err = GenerateRandomStringURLSafe(TokenSize)
	if err != nil {
		return "", err
	}

	t.Id, err = usecase.Repo.NewToken(ctx, t)
	if err != nil {
		return "", err
	}

	return t.Token, nil
}

func (usecase Usecase) GoogleIdToken(
	ctx context.Context,
	idToken string,
) (token string, err error) {
	email, name, err := dataFromIdToken(ctx, idToken, usecase.Audience)
	if err != nil {
		log.Printf("id token: %v", err)
		return "", ThirdPartyError{err: err}
	}

	user, err := usecase.Repo.FindUserByEmail(ctx, email)
	if err != nil && err != ErrNotFound {
		return "", err
	}

	if err == ErrNotFound {
		user = User{
			Email:              email,
			FullName:           name,
			SignedInWithGoogle: true,
		}

		user.Id, err = usecase.Repo.NewUser(ctx, user)
		if err != nil {
			return "", err
		}
	}

	t := Token{
		UserId: user.Id,
	}
	t.Token, err = GenerateRandomStringURLSafe(TokenSize)
	if err != nil {
		return "", err
	}

	t.Id, err = usecase.Repo.NewToken(ctx, t)
	if err != nil {
		return "", err
	}

	return t.Token, nil
}

func dataFromIdToken(ctx context.Context, idToken, audience string) (email, name string, err error) {
	payload, err := idtoken.Validate(ctx, idToken, audience)
	if err != nil {
		return "", "", err
	}

	return payload.Claims["email"].(string), payload.Claims["name"].(string), nil
}

func (usecase Usecase) Login(
	ctx context.Context,
	email, password string,
) (token string, err error) {
	email, err = ValidateEmail(email)
	if err != nil {
		return "", err
	}

	user, err := usecase.Repo.FindUserByEmail(ctx, email)
	if err != nil && err != ErrNotFound {
		return "", err
	}

	if err == ErrNotFound || user.Email != email {
		return "", ErrNotFound
	}

	if !CheckPassword(user.Salt, password, user.HashedPassword) {
		return "", ErrWrongPassword
	}

	t := Token{
		UserId: user.Id,
	}
	t.Token, err = GenerateRandomStringURLSafe(TokenSize)
	if err != nil {
		return "", err
	}

	t.Id, err = usecase.Repo.NewToken(ctx, t)
	if err != nil {
		return "", err
	}

	return t.Token, nil
}

func (usecase Usecase) Authorize(
	ctx context.Context,
	token string,
) (User, error) {
	if len(token) < TokenSize || 2*TokenSize < len(token) {
		return User{}, ErrNotFound
	}

	t, err := usecase.Repo.FindToken(ctx, token)
	if err != nil {
		return User{}, err
	}
	if t.ValidUntil != nil && t.ValidUntil.Before(time.Now()) {
		return User{}, ErrExpired
	}

	return usecase.Repo.FindUserById(ctx, t.UserId)
}

func (usecase Usecase) UpdateUser(
	ctx context.Context,
	user User,
	newEmail, newFullName, newAddress, newTelephone string,
) error {
	newEmail, err := ValidateEmail(newEmail)
	if err != nil {
		return err
	}

	if user.Email != newEmail && user.SignedInWithGoogle {
		return ErrCannotChangeEmail
	}

	user.Email = newEmail
	user.FullName = newFullName
	user.Address = newAddress
	user.Telephone = newTelephone

	return usecase.Repo.UpdateUserData(ctx, user)
}

func (usecase Usecase) SendResetLink(
	ctx context.Context,
	email string,
) error {
	email, err := ValidateEmail(email)
	if err != nil {
		return err
	}

	user, err := usecase.Repo.FindUserByEmail(ctx, email)
	if err != nil {
		return err
	}

	fifteenMinuteLater := time.Now().Add(15 * time.Minute)
	t := Token{
		UserId:     user.Id,
		ValidUntil: &fifteenMinuteLater,
	}
	t.Token, err = GenerateRandomStringURLSafe(TokenSize)
	if err != nil {
		return err
	}

	t.Id, err = usecase.Repo.NewToken(ctx, t)
	if err != nil {
		return err
	}

	return usecase.Mail.Send(user.Email, fmt.Sprintf(
		`To: %s
Subject: [AMKH] Password reset link
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 7bit
X-Auto-Response-Suppress: All

Hi,

In order to reset you password go to: %s%s

If you didn't request a password reset ignore this email.

Best Regards,
AMKH`,
		user.Email, usecase.FrontEndPasswordResetLink, t.Token,
	))
}

func (usecase Usecase) UpdatePassword(
	ctx context.Context,
	userId int, newPassword string,
) error {
	newPassword, err := ValidatePassword(newPassword)
	if err != nil {
		return err
	}

	salt, hashedPassword, err := SaltedHashedPassword(newPassword)
	if err != nil {
		return err
	}

	user := User{
		Id:             userId,
		Salt:           salt,
		HashedPassword: hashedPassword,
	}
	return usecase.Repo.UpdateUserPassword(ctx, user)
}

func (usecase Usecase) Logout(
	ctx context.Context,
	token string,
) {
	if len(token) < TokenSize || 2*TokenSize < len(token) {
		return
	}

	usecase.Repo.ExpireToken(ctx, token)
}
