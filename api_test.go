package main

import (
	"strings"
	"testing"

	"encoding/json"
	"net/http"

	"net/http/httptest"
)

func TestSignup(t *testing.T) {

	api := NewAPI(Usecase{NewRepoMock()})

	t.Run("GET on /signup should not be allowed", func(t *testing.T) {
		w := get(
			&api.Mux,
			"/signup",
		)

		if w.Code != http.StatusMethodNotAllowed {
			t.Errorf("expected %v got %v", http.StatusMethodNotAllowed, w.Code)
			return
		}
	})

	t.Run("POST on /signup with bad email should end with error", func(t *testing.T) {
		mailTester := func(email string) {
			requestData := signUpRequestData{
				Email:    email,
				Password: "a1234567",
			}

			w := post(
				&api.Mux,
				"/signup",
				requestData,
			)

			if w.Code != http.StatusBadRequest {
				t.Errorf("expected %v got %v for %v", http.StatusBadRequest, w.Code, email)
				return
			}
		}

		mailTester("email")
		mailTester("example.com")
		mailTester("@example.com")
	})

	t.Run("POST on /signup with bad password should end with error", func(t *testing.T) {
		passTester := func(password string) {
			requestData := signUpRequestData{
				Email:    "me@example.com",
				Password: password,
			}

			w := post(
				&api.Mux,
				"/signup",
				requestData,
			)

			if w.Code != http.StatusBadRequest {
				t.Errorf("expected %v got %v for %v", http.StatusBadRequest, w.Code, password)
				return
			}
		}

		passTester("1234")
		passTester("1234678")
		passTester("a234")
	})

	t.Run("POST on /signup with correct email and password should return a token", func(t *testing.T) {
		requestData := signUpRequestData{
			Email:    "me@example.com",
			Password: "abcd1234",
		}

		w := post(
			&api.Mux,
			"/signup",
			requestData,
		)

		if w.Code != http.StatusCreated {
			t.Errorf("expected %v got %v", http.StatusCreated, w.Code)
			return
		}

		var data struct {
			Token string `json:"token"`
		}
		if err := json.NewDecoder(w.Body).Decode(&data); err != nil {
			t.Fatal(err)
		}

		if len(data.Token) == 0 {
			t.Fatalf("token should not be empty")
		}

		t.Run("same email should not signup twice", func(t *testing.T) {
			w := post(
				&api.Mux,
				"/signup",
				requestData,
			)

			if w.Code != http.StatusBadRequest {
				t.Errorf("expected %v got %v", http.StatusBadRequest, w.Code)
				return
			}
		})
	})
}

func get(
	handler http.Handler,
	endpoint string,
) *httptest.ResponseRecorder {
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, endpoint, nil)

	handler.ServeHTTP(w, r)

	return w
}

func post(
	handler http.Handler,
	endpoint string,
	data interface{},
) *httptest.ResponseRecorder {
	b, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, endpoint, strings.NewReader(string(b)))

	handler.ServeHTTP(w, r)

	return w
}
